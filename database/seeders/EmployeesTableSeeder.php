<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // Let's truncate our existing records to start from scratch.
        Employee::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 5; $i++) {
            Employee::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'jobTitle' => $faker->jobTitle,
                'phone' => $faker->phoneNumber,
                'imageUrl' => $faker->imageUrl,
            ]);
        }
    }
}
