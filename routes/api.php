<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Employee;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('employees', function() {
    // If the Content-Type and Accept headers are set to 'application/json',
    // this will return a JSON structure. This will be cleaned up later.
    return Employee::all();
});

Route::get('employees/{id}', function($id) {
    return Employee::find($id);
});

Route::post('employees/add', function(Request $request) {
    //return Employee::create($request->all);
    $data = $request->all();
        return Employee::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'jobTitle' => $data['jobTitle'],
            'phone' => $data['phone'],
            'imageUrl' => $data['imageUrl'],
        ]);
});

Route::put('employees/update/{id}', function(Request $request, $id) {
    $employee = Employee::find($id);
    $employee->update($request->all());

    return $employee;
});

Route::delete('employees/delete/{id}', function($id) {
    Employee::where('id',$id)->delete();
    return Employee::all();
});
