<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;

class EmployeeController extends Controller
{
    //
    public function index()
    {

        return Employee::all();
    }

    public function show($id)
    {
        return Employee::find($id);
    }

    public function store(Request $request)
    {
        return Employee::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $article = Employee::findOrFail($id);
        $article->update($request->all());

        return $article;
    }

    public function delete(Request $request, $id)
    {
        $article = Employee::findOrFail($id);
        $article->delete();

        return 204;
    }
}
