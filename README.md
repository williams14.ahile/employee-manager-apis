# Laravel Apis for manager user

Installation commands

    composer install

Generate key

    php artisan key:generate

Clean config cache

    php artisan config:cache

Clean config route

    php artisan route:cache

For the creation of the Employee model

    php artisan migrate

Create records at the bottom of the data

    php artisan db:seed --class=EmployeesTableSeeder

Lauch App

    php artisan serve
