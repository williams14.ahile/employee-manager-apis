FROM php:7.3-fpm-alpine

RUN apk add --update \
&& docker-php-ext-install pdo pdo_mysql
